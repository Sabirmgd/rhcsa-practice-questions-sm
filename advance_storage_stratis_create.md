29

[This is a part of the RHCSA site](https://rhcsapracticequestions.com/questions/questions) 


# Implementing Advanced Storage Features

## Question

Create a thinly provisioned filesystem called **practice-fs** using stratis in a pool named **practice-pool** mounted at **/practice-stratis-volume**

## Background

Stratis is one of the new topics of RHCSA 8, which in short allows you to create a pool that groups block devices together.

For example, create a pool from disk1 which is 5GB and disk2 which is 3GB, the total size now is 8 GB.

In these pools, you can create a filesystem, which can change the size based on the data

So, for example, you create a filesystem fs and you add a large 2GB file, the size of this filesystem will change based on the data and the usage of this filesystem will increase.

So we have Block Devices -> Pools -> File systems

To Practice Advanced Storage features for RHCSA 8, Please add a disk to your VM. I added a disk of 1GB so we can implement the advanced Storage features

```
[vagrant@server1 ~]$ lsblk
NAME                MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                   8:0    0   32G  0 disk
├─sda1                8:1    0    1G  0 part /boot
└─sda2                8:2    0   31G  0 part
  ├─rhel_rhel8-root 253:0    0 28.9G  0 lvm  /
  └─rhel_rhel8-swap 253:1    0  2.1G  0 lvm  [SWAP]
sdb                   8:16   0    5G  0 disk
├─sdb1                8:17   0  256M  0 part
│ └─shazam-storage  253:2    0  700M  0 lvm  /storage
├─sdb2                8:18   0  256M  0 part
│ └─shazam-storage  253:2    0  700M  0 lvm  /storage
├─sdb3                8:19   0  512M  0 part [SWAP]
├─sdb4                8:20   0    1K  0 part
└─sdb5                8:21   0  512M  0 part
  └─shazam-storage  253:2    0  700M  0 lvm  /storage
sdc                   8:32   0    1G  0 disk
```

## Steps

Steps:

- Download required packages
- Create a pool
- Create filesystem
- Mount filesystem

To use stratis, you need to install two packages **stratisd** **stratis-cli**

## Answer

1- install the two packages

```console
[root@server1 ~]# yum install stratisd stratis-cli
```

2- Enable the stratisd service with --now

```console
[root@server1 ~]# systemctl enable --now stratisd
```

3- Create the pool with the specified disk

```console
[root@server1 ~]# stratis pool create practice-pool /dev/sdc
```

(**Note**: if you are asked to create the pool from multiple disks, you could add the rest of the disks as follows):

```console
stratis pool add-data POOL_NAME DEVICE
```

```console
stratis pool add-data practice-pool /dev/sdb
```

4-Verify the pool is created

```console
[root@server1 ~]# stratis pool list
Name             Total Physical Size  Total Physical Used
practice-pool                  1 GiB               52 MiB
```

5- Create the filesystem

```console
[root@server1 ~]# stratis filesystem create practice-pool practice-fs
```

6- Verify the filesystem is created

```console
[root@server1 ~]# stratis filesystem list
Pool Name      Name         Used     Created            Device                              UUID
practice-pool  practice-fs  545 MiB  Mar 02 2020 10:04  /stratis/practice-pool/practice-fs  e9a9032800044fcb98db7517b7322649
```

7- Get the UUID of the filesystem so we could mount it

```console
[root@server1 ~]# lsblk  --output=UUID /stratis/practice-pool/practice-fs
UUID
e9a90328-0004-4fcb-98db-7517b7322649
```

8- Create the mounting point

```console
[root@server1 ~]# mkdir /practice-stratis-volume
```

9- Add the following entry to fstab file

```console
UUID=e9a90328-0004-4fcb-98db-7517b7322649 /practice-stratis-volume xfs defaults,x-systemd.requires=stratisd.service 0 0
```

(**IMPORTANT**) don't forget to add the options appropriately **x-systemd.requires=stratisd.service** is needed for stratis filesystems

10- Mount the filesystem

```console
[root@server1 ~]# mount /practice-stratis-volume
```
