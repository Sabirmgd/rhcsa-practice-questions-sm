36

[This is a part of the RHCSA site](https://rhcsapracticequestions.com/questions/questions) 
# Managing containers

## Question

- Configure `apache-container` as a SYSTEMD service that starts at boot.

## Background

This question goes hand in hand with the previous question, remember in the exam, they want your implementation to survive a re-boot, hence, this question is really important. 



## Answer
For a container to run as a service and start on boot, we need to add the service file to `/etc/systemd/system`

The service file content is as follows:

```
[Unit]
Description=Apache service
Documentation=podman --help

[Service]
Restart=on-failure
ExecStart=/usr/bin/podman start CONTAINER_ID
ExecStop=/usr/bin/podman stop -t 10 CONTAINER_ID
KillMode=none
Type=forking
PIDFile=PID_FILE_LOCATION


[Install]
WantedBy=multi-user.target
```

1- Get the id of the container 

```console
[root@server1 ~]# podman inspect apache-container
```

Output: 
```
[
    {
        "ID": "10a60808c7e2f26c923bdf930f071d438c425b8050316bb967cdbc0c7966b71d",
        "Created": "2021-01-29T15:01:13.241939511Z",
...
```

As we can see, the id is `10a60808c7e2f26c923bdf930f071d438c425b8050316bb967cdbc0c7966b71d` 

The id will be used to create the service file.


2- Get the pid file for this container which we will use as well, its located at `/var/run/containers/storage/overlay-containers/CONTAINER_ID/userdata`

```console
[root@server1 10a60808c7e2f26c923bdf930f071d438c425b8050316bb967cdbc0c7966b71d]# cd /var/run/containers/storage/overlay-containers/10a60808c7e2f26c923bdf930f071d438c425b8050316bb967cdbc0c7966b71d/userdata
```

Check the files and get the pid file:
```console
[root@server1 userdata]# ls
hostname  hosts  pidfile  resolv.conf  run
```

As you can see, the `pidfile`, hang on there, we will finally be able to create the file.

3- Create the file:

```
 [root@server1 userdata]# vi /etc/systemd/system/apache-server.service
```

Now you need to provide the required info as follows:

```
[Unit]
Description=Apache service
Documentation=podman --help

[Service]
Restart=on-failure
ExecStart=/usr/bin/podman start 10a60808c7e2f26c923bdf930f071d438c425b8050316bb967cdbc0c7966b71d
ExecStop=/usr/bin/podman stop -t 10 10a60808c7e2f26c923bdf930f071d438c425b8050316bb967cdbc0c7966b71d
KillMode=none
Type=forking
PIDFile=/var/run/containers/storage/overlay-containers/10a60808c7e2f26c923bdf930f071d438c425b8050316bb967cdbc0c7966b71d/userdata/pidfile


[Install]
WantedBy=multi-user.target
```

As you can see, we used the container id we retrieved in step 2 and the pid file from step 3

4- Reload systemd:

```console
[root@server1 userdata]# systemctl daemon-reload
```

5- Start and enable the service

```console
[root@server1 system]# systemctl start apache-server.service
[root@server1 system]# systemctl enable apache-server.service
Created symlink /etc/systemd/system/multi-user.target.wants/apache-server.service → /etc/systemd/system/apache-server.service.

```


6- Check the status of the service and make sure its running 

```console
.
[root@server1 system]# systemctl status apache-server.service
● apache-server.service - Apache service
   Loaded: loaded (/etc/systemd/system/apache-server.service; enabled; vendor preset: disabled)
   Active: active (running) since Sat 2021-02-06 14:05:28 UTC; 24s ago
 Main PID: 2118 (httpd)
    Tasks: 0 (limit: 11528)
   Memory: 532.0K
   CGroup: /system.slice/apache-server.service
           ‣ 2118 httpd -D FOREGROUND

```

7- Reboot the system and make sure the service is running

```console
[root@server1 system]# systemctl reboot
```

```console
[root@server1 ~]# systemctl status apache-server.service
● apache-server.service - Apache service
   Loaded: loaded (/etc/systemd/system/apache-server.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-02-01 19:41:06 UTC; 4 days ago
  Process: 747 ExecStart=/usr/bin/podman start 10a60808c7e2f26c923bdf930f071d438c425b8050316bb967cdbc0c7966b71d (code=exited, status=0/SUCCESS)
 Main PID: 1288 (httpd)
    Tasks: 0 (limit: 11528)
   Memory: 50.3M
   CGroup: /system.slice/apache-server.service
           ‣ 1288 httpd -D FOREGROUND

Feb 01 19:40:55 server1.eight.example.com systemd[1]: Starting Apache service...
Feb 01 19:41:06 server1.eight.example.com podman[747]: 10a60808c7e2f26c923bdf930f071d438c425b8050316bb967cdbc0c7966b71d
Feb 01 19:41:06 server1.eight.example.com systemd[1]: apache-server.service: Supervising process 1288 which is not our child. We'll most likely not notice when it exits.
Feb 01 19:41:06 server1.eight.example.com systemd[1]: Started Apache service.
```
